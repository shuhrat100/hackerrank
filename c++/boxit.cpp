#include <iostream>

using namespace std;

int BoxesCreated, BoxesDestroyed;

//Implement the class Box
//l,b,h are integers representing the dimensions of the box

// The class should have the following functions :
class Box {
private:
    int l, b, h;
public:
    // Constructors:
    Box() {
        l = 0;
        b = 0;
        h = 0;
        BoxesCreated += 1;
    }

    Box(int length, int breadth, int height) {
        l = length;
        b = breadth;
        h = height;
        BoxesCreated += 1;
    }

    Box(Box &box) {
        l = box.l;
        b = box.b;
        h = box.h;
        BoxesCreated += 1;
    }


    // Destructor
    ~Box() {
        BoxesDestroyed += 1;
    }

    // Return box's length
    int getLength() {
        return l;
    }

    // Return box's breadth
    int getBreadth() {
        return b;
    }

    // Return box's height
    int getHeight() {
        return h;
    }

    // Return the volume of the box
    long long CalculateVolume() {
        return (long long)l * b * h;
    }

    //Overload operator < as specified
    bool operator<(Box &b) {
        if (this->l < b.l || this->b < b.b && this->l == b.l || this->h < b.h && this->b == b.b && this->l == b.l) {
            return true;
        }
        return false;
    }

    //Overload operator << as specified
    friend ostream &operator<<(ostream &out, Box box) {
        out << box.l<< " " << box.b << " " << box.h;
        return out;
    }
};



void check2() {
    int n;
    cin >> n;
    Box temp;
    for (int i = 0; i < n; i++) {
        int type;
        cin >> type;
        if (type == 1) {
            cout << temp;
        }
        if (type == 2) {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            temp = NewBox;
            cout << temp;
        }
        if (type == 3) {
            int l, b, h;
            cin >> l >> b >> h;
            Box NewBox(l, b, h);
            if (NewBox < temp) {
                cout << "Lesser" << endl;
            }
            else {
                cout << "Greater" << endl;
            }
        }
        if (type == 4) {
            cout << temp.CalculateVolume() << endl;
        }
        if (type == 5) {
            Box NewBox(temp);
            cout << NewBox;
        }

    }
}

int main() {
    BoxesCreated = 0;
    BoxesDestroyed = 0;
    check2();
    cout << "Boxes Created : " << BoxesCreated << endl << "Boxes Destroyed : " << BoxesDestroyed << endl;
}