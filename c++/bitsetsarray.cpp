#include <iostream>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

int main() {

    uint32_t N, S, P, Q;

    uint32_t next = 0;

    int offset = pow(2, 31);

    std::cin >> N;

    if (N < 1 || N > 100000000) {
        exit(1);
    }

    std::cin >> S;

    if (S < 0 || S >= offset) {
        exit(1);
    }

    std::cin >> P;

    if (P < 0 || P >= offset) {
        exit(1);
    }
    std::cin >> Q;
    if (Q < 0 || Q >= offset) {
        exit(1);
    }
    uint32_t first = S % offset;

    int counter = 1;

    for (int i = 1; i <= (N - 1); ++i) {

        next = (first * P + Q) % offset;

        if (first != next) {
            std::cout << "first:" << first << "     next:" << next << std::endl;
            counter++;
        }
        first = next;
    }
    std::cout << counter;
    return 0;
}
