
#include <iostream>
#include <sstream>

using namespace std;

/*
    add code for struct here.
*/
class Student {

private:
    int scores[5];

public:

    void input() {
        for (int i = 0; i < 5; ++i) {
            cin >> scores[i];
            if (scores[i]<0||scores[i]>50){
                return;
            }
        }
    }

    int calculateTotalScore() {
        int totalScore = 0;
        for (int i = 0; i < 5; ++i) {
            totalScore += scores[i];
        }
        return totalScore;
    }
};

int main() {

    return 0;
}
