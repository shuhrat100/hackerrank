#include <iostream>
#include <math.h>
#include <tic.h>
//Operator Overloading


using namespace std;

class Person {
protected:
    std::string name;
    int age;
public:
    Person() {

    }

    Person(std::string n, int a) {
        this->name = n;
        this->age = a;
    }

    virtual void getdata() {
        univ_getData();
    }

    virtual void putdata() {
        std::cout << name;
        std::cout << age;
    }

    void univ_getData() {
        std::cin >> name;
        if (name.length() < 1 || name.length() > 100) {
            exit(1);
        }
        std::cin >> age;
        if (age < 1 || age > 80) {
            exit(1);
        }
    }
};


class Professor : public Person {

    int publications;

    int curid = 0;
public:
    static int counter ;
    Professor(){
        ++counter;
        curid = counter;
    }
    void putdata() {
        std::cout << name << " " << age << " " << publications << " " << curid << std::endl;
    }

    void getdata() {

        univ_getData();

        std::cin >> publications;
        if (publications < 1 || publications > 1000) {
            exit(1);
        }
    }
};
int Professor::counter=0;

class Student : public Person {

    int marks[6];

    int curid = 0;

public:
    static int counter ;
    Student(){
        ++counter;
        curid = counter;
    }
    void putdata() {

        int sum = 0;

        for (int i = 0; i < 6; ++i) {
            sum += marks[i];
        }
        std::cout << name << " " << age << " " << sum << " " << curid << std::endl;
    }

    void getdata() {

        univ_getData();

        for (int i = 0; i < 6; ++i) {
            std::cin >> marks[i];
            if (marks[i] < 0 || marks[i] > 100) {
                exit(1);
            }
        }
    }
};
int Student::counter=0;

int main() {

    int n, val;
    cin>>n; //The number of objects that is going to be created.
    Person *per[n];

    for(int i = 0;i < n;i++){

        cin>>val;
        if(val == 1){
            // If val is 1 current object is of type Professor
            per[i] = new Professor;

        }
        else per[i] = new Student; // Else the current object is of type Student

        per[i]->getdata(); // Get the data from the user.

    }

    for(int i=0;i<n;i++)
        per[i]->putdata(); // Print the required output for each object.

    return 0;

}










