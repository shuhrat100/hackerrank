
#include <iostream>
#include <sstream>

using namespace std;

/*
    add code for struct here.
*/
class Student {

private:
    int age;
    char first_name[51];
    char last_name[51];
    int standard;

public:

    int get_age() const {
        return age;
    }

    void set_age(int a) {
        age = a;
    }

    int get_standard() const {
        return standard;
    }

    void set_standard(int standa) {
        standard = standa;
    }

    std::string get_first_name() {
        return first_name;
    }

    void set_first_name(std::string firstn) {
        int size = firstn.length();
        size = size > 51 ? 51 : size;
        for (int i = 0; i < size; i++) {
            first_name[i] = firstn[i];
        }
        first_name[size]='\0';
    }

    std::string get_last_name() {
        return last_name;
    }

    void set_last_name(std::string lastn) {
        int size = lastn.length();
        size = size > 51 ? 51 : size;
        for (int i = 0; i < size; i++) {
            last_name[i] = lastn[i];
        }
        last_name[size]='\0';
    }

    std::string to_string() {

        std::string str("");
        str = std::to_string(age );
        str.append(" ");
        str.append(first_name) ;
        str.append(" ");
        str.append(last_name);
        str.append(" ");
        str.append(std::to_string(standard));
        return str;
    }
};

int main() {

    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << " ";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();


    return 0;
}
